public class main {

    /*Configuration Control Parameters*/
    // Sudoku - General Contract
    private static final int sudoku_dimensions              = 9;  //Should be a Square Number!
    private static final int numbersToRemoveFromGrid        = 41; // 41 - Easy; 56 - Medium; 61 - Hard.
    private static final int maxGenerationsStall            = 100;

    // Genetic Algorithm
    private static final int populationSize                 = 2500;
    private static final double mutationRate                = 0.01;
    private static final double crossoverRate               = 1.0;
    private static final int elitismCount                   = 1;
    private static final int tournamentSize                 = 10;
    private static final double tournamentSelectProbability = 0.95;

    // Island Model
    private static final boolean enableIslandModel          = false;
    private static final int islandSize                     = 10;
    private static final int migrationInterval              = 10;
    private static final int migrationSize                  = 250;

    // Meta-Genetic Algorithm
    private static final boolean enableGAParameterTuning    = false;
    private static final int metaGAPopulationSize           = 5;
    private static final double metaGAMutationRate          = 0.20;
    private static final double metaGACrossoverRate         = 0.75;
    private static final int metaGAElitismCount             = 1;
    private static final int metaGATournamentSize           = 2;
    private static final double metaGATournSelectProb       = 0.95;
    private static final int metaGAMaxGenerationsStall      = 50;
    /*Configuration Control Parameters*/

    public static void main(String[] args) {
        if(enableIslandModel){
            islandModel();
        } else if(enableGAParameterTuning) {
            GAparameterTuning();
        } else {
            sudokuGeneticAlgorithm();
        }
    }

    public static void sudokuGeneticAlgorithm(){
        sudokuGrid grid = SudokuFactory.sudokuFactory(sudoku_dimensions, numbersToRemoveFromGrid);
        grid.print();

        geneticAlgorithm GA = new geneticAlgorithm(populationSize, mutationRate, crossoverRate, elitismCount,
                                                   tournamentSize, tournamentSelectProbability, maxGenerationsStall);

        GA.initPopulation(grid);
        GA.evalPopulation();

        do{
            if(GA.isMaxGenerationsReached()) break;

            System.out.println("Current Generation: " + GA.getCurrentGenerationCount());
            System.out.println("Best Current Solution Fitness: " + GA.getPopulation().getFittest().getFitness());
            System.out.println("Population Fitness: " + GA.getPopulation().getPopulationFitness());

            GA.sudokuUniformCrossover();
            GA.sudokuMutate();
            GA.evalPopulation();
            GA.increaseGenerationCount();
        }while(!GA.isTerminationConditionMet());

        if(!GA.isMaxGenerationsReached()) {
            System.out.println("Solution Found!");
        }

        System.out.println("Total Generation Count: " + GA.getCurrentGenerationCount());
        System.out.println("Final Solution Fitness: " + GA.getPopulation().getFittest().getFitness());
        System.out.println("Final Population Fitness: " + GA.getPopulation().getPopulationFitness());

        grid = GA.getPopulation().getFittest().getIndividual();
        grid.print();
    }

    public static void islandModel(){
        sudokuGrid grid = SudokuFactory.sudokuFactory(sudoku_dimensions, numbersToRemoveFromGrid);
        grid.print();

        IslandModel islandModel = new IslandModel(populationSize, mutationRate, crossoverRate, elitismCount,
                                                  tournamentSize, tournamentSelectProbability,
                                                  islandSize, migrationInterval, migrationSize, maxGenerationsStall);

        islandModel.init(grid);
        islandModel.start_up();

        for(;;) {
            islandModel.awaitForThreads();
            {
                if (islandModel.isArchipelagoReadyForMigration()) {
                    islandModel.sudokuMigration();
                }

                islandModel.evaluateIslands();

                System.out.println("Current Generation: " + islandModel.getGenerationCount());
                System.out.println("Best Current Solution Fitness: " + islandModel.getFittestIndividualByThread().getGA().getPopulation().getFittest().getFitness());
                System.out.println("Island Fitness: " + islandModel.getIslandFitness());

                if (islandModel.isIslandTerminationConditionMet() || islandModel.isMaxGenerationsReached()) {
                    islandModel.releaseThreads();
                    break;
                }

                islandModel.incGenCount();
            }
            islandModel.startNextThreadGeneration();
        }
        if(!islandModel.isMaxGenerationsReached()) {
            System.out.println("Solution Found!");
        }

        System.out.println("Total Generation Count: " + islandModel.getGenerationCount());
        System.out.println("Final Solution Fitness: " + islandModel.getFittestIndividualByThread().getGA().getPopulation().getFittest().getFitness());
        System.out.println("Final Island Fitness: " + islandModel.getIslandFitness());

        grid = islandModel.getFittestIndividualByThread().getGA().getPopulation().getFittest().getIndividual();
        grid.print();
    }

    public static void GAparameterTuning(){

        MetaGeneticAlgorithm MGA = new MetaGeneticAlgorithm(metaGAPopulationSize, metaGAMutationRate, metaGACrossoverRate,
                                                            metaGAElitismCount, metaGATournamentSize, metaGATournSelectProb);

        do {
            sudokuGrid grid = SudokuFactory.sudokuFactory(sudoku_dimensions, numbersToRemoveFromGrid);
            MGA.init_GA(grid);
            MGA.start_up();
            MGA.awaitForThreads();
            MGA.collectResultsFromGA();
            MGA.releaseGAThreads();
            MGA.evalPopulation();

            System.out.println("Current Generation: " + MGA.getCurrentGenerationCount());
            System.out.println("Best Current Solution Fitness: " + MGA.getPopulation().getFittestByIndex(0).getFitness());
            System.out.println("Solution GA Generation Count" + MGA.getPopulation().getFittestByIndex(0).getGAGenerationCount());
            System.out.println("Solution GA Fitness Value" + MGA.getPopulation().getFittestByIndex(0).getFitnessGA());
            System.out.println("Population Size: " + MGA.getPopulation().getFittestByIndex(0).populationSize);
            System.out.println("Crossover Rate: " + MGA.getPopulation().getFittestByIndex(0).crossoverRate);
            System.out.println("Mutation Rate: " + MGA.getPopulation().getFittestByIndex(0).mutationRate);

            MGA.AverageValueCrossover();
            MGA.Mutation();
            MGA.incGenCount();
        } while (!MGA.isTerminationConditionMet());
    }

    public static geneticAlgorithm sudokuGeneticAlgorithmForTuning(int pS, double mR, double cR){
        sudokuGrid grid = SudokuFactory.sudokuFactory(sudoku_dimensions, numbersToRemoveFromGrid);

        geneticAlgorithm GA = new geneticAlgorithm(pS, mR, cR, elitismCount, tournamentSize, tournamentSelectProbability, metaGAMaxGenerationsStall);

        GA.initPopulation(grid);

        do{
            if(GA.isMaxGenerationsReached()) break;
            GA.evalPopulation();

            GA.sudokuUniformCrossover();
            GA.sudokuMutate();
            GA.increaseGenerationCount();
        }while(!GA.isTerminationConditionMet());

        return GA;
    }

}